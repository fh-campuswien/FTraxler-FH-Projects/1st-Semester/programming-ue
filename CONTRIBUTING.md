## Feel free to contribute!  
Please log in or register. I will add you to the project.  
  
## STRUCTURE:  
Only use *english* language in your code!
Each homework or lesson should be in a new folder.  
Do not upload any description docs or image files. Only source code!  
Please do not add binaries.  
  
When having problems with anything, feel free to create an issue on the issue board.