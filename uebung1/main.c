#include "stdio.h"
#define _CRT_SECURE_NO_WARNINGS

void ex_one()
{
    printf("Hello World\n");
}

void ex_two()
{
    int a = 0, b = 0;
    printf("Enter a numer:");
    if (scanf("%i", &a))
        printf("Thaks for number %i\n", a);
    else
    {
        printf("ERROR: Inalid input!\n");
        goto endexfour;
    }
    printf("Enter another number to add to the first:");
    if (scanf("%i", &b))
        printf("Thaks for number %i\n", b);
    else
    {
        printf("ERROR: Inalid input!\n");
        goto endexfour;
    }
    printf("The result is: %d\n", a + b);
    endexfour:getchar();
}

void ex_three()
{
    printf("Converting Celsius to Farenheit.\n");
    float cw = 0;
    printf("Please enter a celsius value:");
    scanf("%f", &cw);
    float fw = cw * 9 / 5 + 32;
    printf("Corresponding Farenheit value: %f\n", fw);
    getchar();
}

void ex_four()
{
    printf("Validating ZIP-code:");
    int zip = 0;
    if(scanf("%i", &zip))
    {
        if(zip >= 1000 && zip <= 9999)
            printf("The ZIP-code is valid.\n");
        else
            printf("This ZIP-code is not valid!\n");
    }
    else
        printf("ERROR: Inalid input!\n");
    getchar();
}

void ex_five()
{
    printf("Enter numbers until \"0\" is entered. Then the sum will be displayed.\n");
    int sum = 0;
    while(1) // alles außer 0 ist wahr
    {
        int input = 0;
        if(scanf("%i", &input))
        {
            if(input != 0)
            {
                sum += input;
                //printf("actual sum: %i\n", sum);
            }
            else
                break;
        }
        else 
            printf("ERROR: Invalid input.");
    }
    printf("The sum is: %i", sum);
    getchar();
}

int main()
{
    //ex_one();
    //ex_two();
    //ex_three();
    //ex_four();
    ex_five();
    // pause
    printf("\n");
    printf("Press ENTER key to Continue\n");
    getchar();
}